const { xml, jid } = require("@xmpp/client")
const { Client } = require("@hool/client")

class FaekoClient extends Client {

  constructor(options = {}, ...args) {
    // set the standard
    options.standard = 'http://hool.org/protocol/mug/faeko'

    // initialise with parent constructor
    super(options, ...args)
  }

  /**
   * Game-level listeners
   */
  handleGamePresence(presence) {
    let game = presence.getChild("game")

    if (game.attrs.xmlns == 'http://jabber.org/protocol/mug') {

      // Handle game errors
      if (presence.getChildren('error').length) {

        let result = {
          room: jid(presence.attrs.from).bare(),
          user: jid(presence.attrs.to).bare(),
          error: {}
         }
        let error = presence.getChild('error')

        if (error.attrs.type == 'wait') {
          result.error.type = 'wait'

          // Handle "room full" event
          if (error.getChildren('service-unavailable').length) {
            if (
              error
              .getChild('service-unavailable')
              .attrs
              .xmlns == 'urn:ietf:params:xml:ns:xmpp-stanzas'
            ) {
              result.event = 'roomFull'
              return result
            }
          }
        }
      }

      // Handle state update
      let status
      if (game.getChildren('status').length) {
        status = game.getChild('status')
      }

      if (status || game.getChildren('state').length) {
        let state = {}

        state.room = jid(presence.attrs.from).bare().toString()

        // add in the status
        if (status) state.status = status.text()

        let xData = (game.getChild('state')
          .getChildren('x')
          .find((x) => x.attrs.xmlns == 'jabber:x:data'))

        if (xData) {
          // Now the actual processing begins!

          // get deck info
          if (!!xData.getChildren('deck').length) {
            let deckEl = xData.getChild('deck')
            state.deck = []

            // trump
            if (deckEl.attrs.trump) {
              state.trump = deckEl.attrs.trump
            }

            // cards
            deckEl.children.forEach((cardEl) =>
              state.deck.push({
                suit: cardEl.attrs.suit,
                rank: cardEl.attrs.rank,
              })
            )
          }

          // get player info
          if (!!xData.getChildren('players').length) {
            let playersEl = xData.getChild('players')
            state.players = []

            playersEl.getChildren('player').forEach((playerEl, i) => {
              let player = {}

              if (playerEl.attrs.inactive == 'true') {
                player.inactive = true
              } else if (playerEl.attrs.inactive == 'false') {
                player.inactive = false
              }
              // get hand
              if (playerEl.getChildren('hand').length) {
                let handEl = playerEl.getChild('hand')
                player.hand = []

                // whether the hand is exposed
                if (handEl.attrs.exposed == 'true') {
                  player.exposed = true
                } else if (handEl.attrs.exposed == 'false') {
                  player.exposed = false
                }

                // the hand itself!
                handEl.getChildren('card').forEach(c =>
                  player.hand.push({
                    suit: c.attrs.suit,
                    rank: c.attrs.rank,
                  }))
              }

              // get bid
              if (playerEl.getChildren('bid').length) {
                let bidEl = playerEl.getChild('bid')

                player.bid = Number(bidEl.text())

                // If it's empty, make it "true". That means
                // we know there's a bid but don't know what
                // it is.
                if (isNaN(player.bid)) {
                  player.bid = true
                }
              }

              // get score (if any)
              if (playerEl.getChildren('score').length) {
                let scoreEl = playerEl.getChild('score')

                player.score = Number(scoreEl.text())
              }

              // respect the index, if specified
              if (player.index) i = player.index

              // add the player to the list
              state.players[i] = player
            })

            // get trick info
            if (!!xData.getChildren('tricks').length) {
              let tricksEl = xData.getChild('tricks')
              state.tricks = []
              state.flippedTricks = []

              tricksEl.getChildren('trick').forEach((trickEl) => {
                let trick = []

                trickEl.getChildren('card').forEach((cardEl) => {
                  trick.push({
                    suit: cardEl.attrs.suit,
                    rank: cardEl.attrs.rank,
                    player: cardEl.attrs.player,
                  })
                })

                state.tricks.push(trick)

                // Check the flip value
                if (!!trickEl.attrs.flipped) {
                  state.flippedTricks.push(true)
                } else {
                  state.flippedTricks.push(false)
                }

              })
              //setting trump which is come from the presence to the STATE.TRICKS
              let trump = tricksEl.getChildren('trump')[0]
              state.tricks.push(trump);
            }
          }
        }

        // Return it
        state.event = 'state'
        return state
      }

    }

    return
  }

  handleGameTurn(message) {
    let turn = message.getChild('turn')

    // Handle bid
    if (turn.getChildren('bid').length) {
      let bidEl = turn.getChild('bid')

      // Ignore non-Faeko bids
      if (bidEl.attrs.xmlns != this.standard) return

      // Detect errors
      if (message.getChildren('error').length) {
        let errorEl = message.getChild('error')

        let error = {}

        // Figure out error type
        if (errorEl.getChildren('forbidden').length) {
          error.type = 'not-authorised'
        } else if (errorEl.getChildren('invalid-turn').length) {
          error.type = 'invalid-turn'
        } else {
          error.type = 'unknown'
        }

        // Get error text
        if (errorEl.getChildren('text').length) {
          error.text = errorEl.getChild('text').text()
        }

        error.event = 'bidError'
        return error
      }

      // If no errors, return the normal bid!
      let bid = {}
      bid.room = jid(message.attrs.from).bare().toString()
      bid.nickname = jid(message.attrs.from).resource
      bid.player = bidEl.attrs.player

      // It could be either an actual value (integer but
      // passed as a string) or just an indicator saying
      // that *some* bid has been made but not telling us
      // which one.
      if (!!bidEl.text()) {
        bid.value = Number(bidEl.text())
      } else {
        bid.value = true
      }

      bid.event = 'bid'
      return bid
    }

    // Handle card
    if (turn.getChildren('card').length) {
      let cardEl = turn.getChild('card')

      // Ignore non-Faeko cards
      if (cardEl.attrs.xmlns != this.standard) return

      // Detect errors
      if (message.getChildren('error').length) {
        let errorEl = message.getChild('error')

        let error = {}

        // Figure out error type
        if (errorEl.getChildren('forbidden').length) {
          error.type = 'not-authorised'
        } else if (errorEl.getChildren('invalid-turn').length) {
          error.type = 'invalid-turn'
        } else {
          error.type = 'unknown'
        }

        // Get error text
        if (errorEl.getChildren('text').length) {
          error.text = errorEl.getChild('text').text()
        }

        error.event = 'cardError'
        return error
      }

      // If no errors, return the normal card!
      let card = {}
      card.room = jid(message.attrs.from).bare().toString()
      card.nickname = jid(message.attrs.from).resource
      card.rank = cardEl.attrs.rank
      card.suit = cardEl.attrs.suit
      card.player = cardEl.attrs.player

      card.event = 'card'
      return card
    }

    // Handle take
    if (turn.getChildren('take').length) {
      let takeEl = turn.getChild('take')

      // Ignore non-Faeko takes
      if (takeEl.attrs.xmlns != this.standard) return

      // Detect errors
      if (message.getChildren('error').length) {
        let errorEl = message.getChild('error')

        let error = {}

        // Figure out error type
        if (errorEl.getChildren('forbidden').length) {
          error.type = 'not-authorised'
        } else if (errorEl.getChildren('invalid-turn').length) {
          error.type = 'invalid-turn'
        } else {
          error.type = 'unknown'
        }

        // Get error text
        if (errorEl.getChildren('text').length) {
          error.text = errorEl.getChild('text').text()
        }

        error.event = 'takeError'
        return error
      }

      // If no errors, return the normal take!
      let take = {}
      take.room = jid(message.attrs.from).bare().toString()
      take.nickname = jid(message.attrs.from).resource

      take.event = 'take'
      return take
    }

    // Handle flip
    if (turn.getChildren('flip').length) {
      let flipEl = turn.getChild('flip')

      // Ignore non-Faeko flips
      if (flipEl.attrs.xmlns != this.standard) return

      // Detect errors
      if (message.getChildren('error').length) {
        let errorEl = message.getChild('error')

        let error = {}

        // Figure out error type
        if (errorEl.getChildren('forbidden').length) {
          error.type = 'not-authorised'
        } else if (errorEl.getChildren('invalid-turn').length) {
          error.type = 'invalid-turn'
        } else {
          error.type = 'unknown'
        }

        // Get error text
        if (errorEl.getChildren('text').length) {
          error.text = errorEl.getChild('text').text()
        }

        error.event = 'flipError'
        return error
      }

      // If no errors, return the normal flip!
      let flip = {}
      flip.room = jid(message.attrs.from).bare().toString()
      flip.nickname = jid(message.attrs.from).resource

      flip.event = 'flip'
      return flip
    }

  }


  /**
   * Game-level operations
   *
   * Bidding, cardplay, and other operations
   */


  bid(gameID, value) {
    if (!gameID) throw 'Please specify a gameID'
    if(typeof(value) != 'number') throw 'Please specify the bid value'

    // auto-set domain
    if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <bid xmlns='http://hool.org/protocol/mug/faeko'>
            {value}
           </bid>
        </turn>
      </message>
    )
  }

  card(gameID, suit, rank, player=undefined) {
    if (!gameID) throw 'Please specify a gameID'
    if (!suit) throw 'Please specify a suit'
    if (!rank) throw 'Please specify a rank'

    // auto-set domain
    if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <card
            suit={suit}
            rank={rank}
            xmlns={this.standard}/>
        </turn>
      </message>
    )
  }

  take(gameID) {
    if (!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <take xmlns={this.standard}/>
        </turn>
      </message>
    )
  }

  flip(gameID) {
    if (!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}
        type='chat'>
        <turn xmlns='http://jabber.org/protocol/mug#user'>
          <flip xmlns={this.standard}/>
        </turn>
      </message>
    )
  }
}


module.exports = {
  FaekoClient,
}



